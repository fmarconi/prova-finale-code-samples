package it.polimi.ingsw.lab5.prisoners.client.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

import it.polimi.ingsw.lab5.prisoners.server.controller.Change;

public interface ClientViewRemote 
extends Remote {

	public void updateClient(Change c) 
			throws RemoteException;
}
