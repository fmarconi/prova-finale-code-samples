package it.polimi.ingsw.lab5.prisoners.client.rmi;

import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

import it.polimi.ingsw.lab5.prisoners.server.controller.action.Action;
import it.polimi.ingsw.lab5.prisoners.server.controller.action.PrintModel;
import it.polimi.ingsw.lab5.prisoners.server.controller.action.Scommetti;
import it.polimi.ingsw.lab5.prisoners.server.controller.action.TurnOff;
import it.polimi.ingsw.lab5.prisoners.server.controller.action.TurnOn;
import it.polimi.ingsw.lab5.prisoners.server.controller.query.GetPrigionieri;
import it.polimi.ingsw.lab5.prisoners.server.controller.query.Query;
import it.polimi.ingsw.lab5.prisoners.server.view.RMIView;
import it.polimi.ingsw.lab5.prisoners.server.view.RMIViewRemote;

public class ClientRMI {
	private final static int RMI_PORT = 52365;

	
	private final static String HOST = "127.0.0.1";

	private final static int PORT = 52365;

	private static final String NAME = "prigionieri";

	public static void main(String[] args) throws RemoteException, NotBoundException, AlreadyBoundException {

		Registry registry = LocateRegistry.getRegistry(HOST, PORT);
		RMIViewRemote serverStub = (RMIViewRemote) registry.lookup(NAME);

		ClientRMIView rmiView=new ClientRMIView();
		
		serverStub.registerClient(rmiView);
		
		
		Scanner stdIn = new Scanner(System.in);

		while (true) {

			String inputLine = stdIn.nextLine();
			System.out.println("SENDING"+inputLine);
			Action action;
			Query query;
			try {

				switch (inputLine) {
				case "ON":
					action = new TurnOn();
					serverStub.eseguiAzione(action);
					break;
				case "OFF":
					action = new TurnOff();
					serverStub.eseguiAzione(action);
					break;
				case "PRINT":
					action = new PrintModel();
					serverStub.eseguiAzione(action);
					break;
				case "SCOMMETTI":
					action = new Scommetti();
					serverStub.eseguiAzione(action);
					break;
				
				default:
					break;
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}

}
