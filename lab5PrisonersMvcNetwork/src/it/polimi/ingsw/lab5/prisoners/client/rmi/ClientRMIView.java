package it.polimi.ingsw.lab5.prisoners.client.rmi;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import it.polimi.ingsw.lab5.prisoners.server.controller.Change;

public class ClientRMIView extends UnicastRemoteObject implements ClientViewRemote, Serializable{

	protected ClientRMIView() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6111979881550001331L;

	@Override
	public void updateClient(Change c) throws RemoteException {
		System.out.println(c);
	}
}
