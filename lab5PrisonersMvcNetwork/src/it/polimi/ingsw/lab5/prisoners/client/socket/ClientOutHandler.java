package it.polimi.ingsw.lab5.prisoners.client.socket;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import it.polimi.ingsw.lab5.prisoners.server.controller.action.Action;
import it.polimi.ingsw.lab5.prisoners.server.controller.action.PrintModel;
import it.polimi.ingsw.lab5.prisoners.server.controller.action.Scommetti;
import it.polimi.ingsw.lab5.prisoners.server.controller.action.TurnOff;
import it.polimi.ingsw.lab5.prisoners.server.controller.action.TurnOn;
import it.polimi.ingsw.lab5.prisoners.server.controller.query.GetPrigionieri;
import it.polimi.ingsw.lab5.prisoners.server.controller.query.Query;

public class ClientOutHandler implements Runnable {

	private ObjectOutputStream socketOut;

	public ClientOutHandler(ObjectOutputStream socketOut) {
		this.socketOut = socketOut;
	}

	@Override
	public void run() {

		System.out.println("RUNNING");
		Scanner stdIn = new Scanner(System.in);

		while (true) {

			String inputLine = stdIn.nextLine();

			Action action;
			Query query;
			try {

				switch (inputLine) {
				case "ON":
					action = new TurnOn();
					socketOut.writeObject(action);
					socketOut.flush();
					break;
				case "OFF":
					action = new TurnOff();
					socketOut.writeObject(action);
					socketOut.flush();
					break;
				case "PRINT":
					action = new PrintModel();
					socketOut.writeObject(action);
					socketOut.flush();
					break;
				case "SCOMMETTI":
					action = new Scommetti();
					socketOut.writeObject(action);
					socketOut.flush();
					break;
				case "GETPRIGIONIERI":
					query = new GetPrigionieri();
					socketOut.writeObject(query);
					socketOut.flush();
					break;
				default:
					break;
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
	}
}
