package it.polimi.ingsw.lab5.prisoners.server.controller.query;

import java.io.Serializable;

import it.polimi.ingsw.lab5.prisoners.server.model.Model;

public abstract class Query<O> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6852387886693073989L;
	
	public abstract O perform(Model m);
}
