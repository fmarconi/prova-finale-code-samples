package it.polimi.ingsw.lab5.prisoners.server.controller.action;

import it.polimi.ingsw.lab5.prisoners.server.model.Model;
import it.polimi.ingsw.lab5.prisoners.server.model.Switch;

public class TurnOff extends Action {


	/**
	 * 
	 */
	private static final long serialVersionUID = 2845874998488731451L;

	@Override
	public void esegui(Model model) {
		model.setInterruttore(Switch.OFF);
		this.changePrigionero(model);
		
	}
}
