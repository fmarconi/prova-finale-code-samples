package it.polimi.ingsw.lab5.prisoners.server.controller;

import it.polimi.ingsw.lab5.prisoners.server.model.Switch;

public class InterruttoreChange extends Change {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5569590624469139095L;
	private final Switch switchState;
	
	public InterruttoreChange(Switch switchState){
		this.switchState=switchState;
	}

	public Switch getSwitchState() {
		return switchState;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "InterruttoreChange [switchState=" + switchState + "]";
	}
}
