package it.polimi.ingsw.lab5.prisoners.server.view;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Set;

import it.polimi.ingsw.lab5.prisoners.server.controller.Change;
import it.polimi.ingsw.lab5.prisoners.server.controller.action.Action;
import it.polimi.ingsw.lab5.prisoners.server.controller.query.GetPrigionieri;
import it.polimi.ingsw.lab5.prisoners.server.controller.query.Query;
import it.polimi.ingsw.lab5.prisoners.server.model.Model;
import it.polimi.ingsw.lab5.prisoners.server.model.Prigioniero;

public class ServerSocketView extends View implements Runnable {

	private Socket socket;
	private ObjectInputStream socketIn;
	private ObjectOutputStream socketOut;
	private Model model;

	public ServerSocketView(Socket socket, Model model) throws IOException {
		this.socket = socket;
		this.socketIn = new ObjectInputStream(socket.getInputStream());
		this.socketOut = new ObjectOutputStream(socket.getOutputStream());
		this.model=model;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
	}

	@Override
	public void update(Change o) {
		System.out.println("Sending to the client " + o);

		try {
			this.socketOut.writeObject(o);
			this.socketOut.flush();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		while (true) {

			try {

				Object object = socketIn.readObject();
				if (object instanceof Action) {
					Action action = (Action) object;
					System.out.println("VIEW: received the action " + action);

					this.notifyObserver(action);
				}
				if (object instanceof GetPrigionieri) {
					GetPrigionieri query = (GetPrigionieri) object;
					System.out.println("VIEW: received the query " + query);
					Set<Prigioniero> prigionieri=query.perform(model);
					System.out.println(prigionieri);
					this.socketOut.writeObject(prigionieri);
					this.socketOut.flush();

				}

			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
