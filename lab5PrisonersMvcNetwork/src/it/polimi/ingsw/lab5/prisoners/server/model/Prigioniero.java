package it.polimi.ingsw.lab5.prisoners.server.model;

import java.io.Serializable;

public class Prigioniero implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2148390021243945668L;

	// TO ADD
	@Override
	public String toString() {
		return "Prigioniero [name=" + name + "]";
	}

	/**
	 * the name of the prisoner
	 */
	//	private transient String name;
	private transient String name;
	
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * creates a new prisoner
	 * @param name the name of the prisoner
	 * @throws NullPointerException if the name is null
	 */
	public Prigioniero(String name){
		if(name==null){
			throw new NullPointerException("The name cannot be null");
		}
		this.name=name;
	}

	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prigioniero other = (Prigioniero) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}
