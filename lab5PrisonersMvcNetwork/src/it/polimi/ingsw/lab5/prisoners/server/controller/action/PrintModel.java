package it.polimi.ingsw.lab5.prisoners.server.controller.action;

import it.polimi.ingsw.lab5.prisoners.server.model.Model;

public class PrintModel extends Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 400509764316015968L;

	@Override
	public void esegui(Model model) {
		System.out.println(model.toString());

	}

}
