package it.polimi.ingsw.lab5.prisoners.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import it.polimi.ingsw.lab5.prisoners.server.controller.Controller;
import it.polimi.ingsw.lab5.prisoners.server.model.Model;
import it.polimi.ingsw.lab5.prisoners.server.view.RMIView;
import it.polimi.ingsw.lab5.prisoners.server.view.RMIViewRemote;
import it.polimi.ingsw.lab5.prisoners.server.view.ServerSocketView;

public class Server {

	private final static int PORT = 29999;

	private final String NAME = "prigionieri";
	private final static int RMI_PORT = 52365;

	private Model gioco;

	private Controller controller;

	public Server() {
		this.gioco = new Model();
		this.controller = new Controller(gioco);

	}

	private void startRMI() throws RemoteException, AlreadyBoundException{
		
		Registry registry = LocateRegistry.createRegistry(RMI_PORT);
		System.out.println("Constructing the RMI registry");
		RMIView rmiView=new RMIView();
		rmiView.registerObserver(this.controller);
		this.gioco.registerObserver(rmiView);
		
		RMIViewRemote viewRemote=(RMIViewRemote) UnicastRemoteObject.
				exportObject(rmiView, 0);
		
		System.out.println("Binding the server implementation to the registry");
		registry.bind(NAME, rmiView);

		
	}
	
	private void startSocket() throws IOException {

		
		ExecutorService executor = Executors.newCachedThreadPool();

		ServerSocket serverSocket = new ServerSocket(PORT);

		System.out.println("SERVER SOCKET READY ON PORT" + PORT);

		while (true) {
			Socket socket = serverSocket.accept();

			ServerSocketView view = new ServerSocketView(socket, this.gioco);
			this.gioco.registerObserver(view);
			view.registerObserver(this.controller);
			executor.submit(view);
		}
	}

	public static void main(String[] args) throws IOException, AlreadyBoundException {
		Server server = new Server();
		System.out.println("START RMI");
		server.startRMI();
		System.out.println("START SOCKET");
		server.startSocket();
	}
}
