package it.polimi.ingsw.lab5.prisoners.server.controller.action;

import it.polimi.ingsw.lab5.prisoners.server.model.Model;
import it.polimi.ingsw.lab5.prisoners.server.model.State;

public class Scommetti extends Action {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3424745854021444150L;

	@Override
	public void esegui(Model model) {
		if(model.getPrigionieriInTheRoom().equals(model.getPrigionieri())){
			model.setGameState(State.WIN);
		}
		else{
			model.setGameState(State.LOSE);
		}
	}

}
