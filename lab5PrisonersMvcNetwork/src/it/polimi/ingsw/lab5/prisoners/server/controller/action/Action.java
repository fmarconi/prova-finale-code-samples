package it.polimi.ingsw.lab5.prisoners.server.controller.action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.polimi.ingsw.lab5.prisoners.server.model.Model;
import it.polimi.ingsw.lab5.prisoners.server.model.Prigioniero;

public abstract class Action implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -4334191184999875154L;

	public abstract void esegui(Model model);
	
	public void  changePrigionero(Model model) {
		List<Prigioniero> list = new ArrayList<Prigioniero>(model.getPrigionieri());
		Collections.shuffle(list);
		model.setCurrentPrigioniero(list.get(0));
	}
}
