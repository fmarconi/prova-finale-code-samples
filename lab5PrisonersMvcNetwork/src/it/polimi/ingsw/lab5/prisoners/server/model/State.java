package it.polimi.ingsw.lab5.prisoners.server.model;

public enum State {
	RUNNING,
	WIN,
	LOSE
}
