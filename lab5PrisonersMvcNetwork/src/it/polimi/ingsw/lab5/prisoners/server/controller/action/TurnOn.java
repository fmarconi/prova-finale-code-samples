package it.polimi.ingsw.lab5.prisoners.server.controller.action;

import it.polimi.ingsw.lab5.prisoners.server.model.Model;
import it.polimi.ingsw.lab5.prisoners.server.model.Switch;

public class TurnOn extends Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8814283936673886376L;

	@Override
	public void esegui(Model model) {
		model.setInterruttore(Switch.ON);
		this.changePrigionero(model);
	}
	

}
