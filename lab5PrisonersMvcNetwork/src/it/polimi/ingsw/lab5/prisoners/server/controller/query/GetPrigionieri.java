package it.polimi.ingsw.lab5.prisoners.server.controller.query;

import java.util.Set;

import it.polimi.ingsw.lab5.prisoners.server.model.Model;
import it.polimi.ingsw.lab5.prisoners.server.model.Prigioniero;

public class GetPrigionieri extends Query<Set<Prigioniero>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5438964261850156287L;

	@Override
	public Set<Prigioniero> perform(Model m) {
		System.out.println("PERFORMING THE QUERY");
		return m.getNotAssignedPrigionieri();
	}

	
}
