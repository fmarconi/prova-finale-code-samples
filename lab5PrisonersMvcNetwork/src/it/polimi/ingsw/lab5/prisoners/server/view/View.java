package it.polimi.ingsw.lab5.prisoners.server.view;

import it.polimi.ingsw.lab5.prisoners.server.controller.Change;
import it.polimi.ingsw.lab5.prisoners.server.controller.action.Action;
import it.polimi.ingsw.lab5.prisoners.server.observer.Observable;
import it.polimi.ingsw.lab5.prisoners.server.observer.Observer;

public abstract class View extends Observable<Action>
	implements Observer<Change>
	{

}
