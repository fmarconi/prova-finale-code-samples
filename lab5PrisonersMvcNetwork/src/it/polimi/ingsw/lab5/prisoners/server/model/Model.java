package it.polimi.ingsw.lab5.prisoners.server.model;

import java.util.HashSet;
import java.util.Set;

import it.polimi.ingsw.lab5.prisoners.server.controller.Change;
import it.polimi.ingsw.lab5.prisoners.server.controller.InterruttoreChange;
import it.polimi.ingsw.lab5.prisoners.server.controller.StateChange;
import it.polimi.ingsw.lab5.prisoners.server.observer.Observable;

public class Model extends Observable<Change> {

	private Set<Prigioniero> notAssignedPrigionieri;
	private Set<Prigioniero> prigionieri;

	private Set<Prigioniero> prigionieriInTheRoom;

	private Prigioniero currentPrigioniero;

	private State gameState;

	private Switch interruttore;

	public Model() {
		this.prigionieri = new HashSet<>();
		this.prigionieriInTheRoom = new HashSet<>();
		this.prigionieri.add(new Prigioniero("Carlo"));
		this.prigionieri.add(new Prigioniero("Pierluigi"));
		this.prigionieri.add(new Prigioniero("Luca"));
		this.notAssignedPrigionieri=new HashSet<>(this.prigionieri);
		this.setInterruttore(Switch.OFF);
		this.setGameState(State.RUNNING);
		this.setCurrentPrigioniero(this.prigionieri.iterator().next());
	}

	public Set<Prigioniero> getPrigionieri() {
		return this.prigionieri;
	}

	public Set<Prigioniero> getPrigionieriInTheRoom() {
		return this.prigionieriInTheRoom;
	}

	public State getGameState() {
		return gameState;
	}

	public void setGameState(State gameState) {

		this.gameState = gameState;
		this.notifyObserver(new StateChange(this.gameState));
	}

	public Switch getInterruttore() {
		return interruttore;
	}

	public void setInterruttore(Switch interruttore) {
		this.interruttore = interruttore;
		this.notifyObserver(new InterruttoreChange(this.interruttore));
	}

	public Prigioniero getCurrentPrigioniero() {
		return currentPrigioniero;
	}

	public void setCurrentPrigioniero(Prigioniero currentPrigioniero) {
		this.currentPrigioniero = currentPrigioniero;
		this.prigionieriInTheRoom.add(currentPrigioniero);
		this.notifyObserver();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Model\n" + " [prigionieri=" + prigionieri + "\n" + "prigionieriInTheRoom=" + prigionieriInTheRoom + "\n"
				+ "currentPrigioniero=" + currentPrigioniero + "\n" + " gameState=" + gameState + "\n" + "interruttore="
				+ interruttore + "]";
	}

	public Set<Prigioniero> getNotAssignedPrigionieri() {
		System.out.println("RETURNING PRIGIONIERI");
		return notAssignedPrigionieri;
	}

}
