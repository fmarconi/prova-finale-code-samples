package it.polimi.ingsw.lab5.prisoners.server.view;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import it.polimi.ingsw.lab5.prisoners.client.rmi.ClientViewRemote;
import it.polimi.ingsw.lab5.prisoners.server.controller.Change;
import it.polimi.ingsw.lab5.prisoners.server.controller.action.Action;

public class RMIView extends View implements RMIViewRemote {

	private Set<ClientViewRemote> clients;

	public RMIView() {
		this.clients = new HashSet<>();
	}

	@Override
	public void registerClient(ClientViewRemote clientStub) throws RemoteException {
		System.out.println("CLIENT REGISTRATO");
		this.clients.add(clientStub);
	}

	@Override
	public void update(Change o) {
		System.out.println("SENDING THE CHANGE TO THE CLIENT");
		try {
			for (ClientViewRemote clientstub : this.clients) {
				
				clientstub.updateClient(o);
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub

	}

	@Override
	public void eseguiAzione(Action action) throws RemoteException {
		System.out.println("I am the RMI view I am notifying my observers");
		this.notifyObserver(action);
	}

}
