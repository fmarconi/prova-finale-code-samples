package it.polimi.ingsw.lab5.prisoners.server.controller;

import it.polimi.ingsw.lab5.prisoners.server.controller.action.Action;
import it.polimi.ingsw.lab5.prisoners.server.model.Model;
import it.polimi.ingsw.lab5.prisoners.server.observer.Observer;

public class Controller implements Observer<Action>{
	
	private final Model gioco;
	
	public Controller(Model gioco){
		this.gioco=gioco;
	}
	
	public void update(Action action){
		System.out.println("I AM THE CONTROLLER UPDATING THE MODEL");
		Observer.super.update(action);
		action.esegui(gioco);
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}
	
	
}
