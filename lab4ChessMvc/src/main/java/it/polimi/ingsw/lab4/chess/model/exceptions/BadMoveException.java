package it.polimi.ingsw.lab4.chess.model.exceptions;

/**
 * Created by marcofunaro on 4/18/15.
 */
public class BadMoveException extends ChessException {
    public BadMoveException(String message) {
        super(message);
    }
}
