package it.polimi.ingsw.lab4.chess.controller;

import it.polimi.ingsw.lab4.chess.model.ChessBoard;
import it.polimi.ingsw.lab4.chess.model.Position;
import it.polimi.ingsw.lab4.chess.model.exceptions.BadPositionException;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by marcofunaro on 4/18/15.
 */
public class Controller {

    private final ChessBoard board;

    public Controller(ChessBoard chessBoard) {
        board = chessBoard;
    }

    public void move(String fromCol, String fromRow, String toCol, String toRow){

        if(!StringUtils.isNumeric(fromRow)) throw new BadPositionException("fromRow must be a number");
        if(!StringUtils.isNumeric(toRow)) throw new BadPositionException("toRow must be a number");
        if(fromCol.length() > 1) throw new BadPositionException("fromCol must be only 1 char long");
        if(toCol.length() > 1) throw new BadPositionException("toCol must be only 1 char long");


        Position from = new Position(fromCol.charAt(0), Integer.parseInt(fromRow));
        Position to = new Position(toCol.charAt(0), Integer.parseInt(toRow));

        board.move(from, to);
    }
}
