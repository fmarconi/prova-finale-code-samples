package it.polimi.ingsw.lab4.chess.view;

import it.polimi.ingsw.lab4.chess.controller.Controller;
import it.polimi.ingsw.lab4.chess.model.ChessBoard;

/**
 * Created by marcofunaro on 4/18/15.
 */
public class Main {
    public static void main(String[] args){
        EventWriter writer = new EventWriter();
        ChessBoard board = new ChessBoard();
        board.register(writer);
        new CommandReader(new Controller(board)).startReading();
    }

}
