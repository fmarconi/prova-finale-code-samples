package it.polimi.ingsw.lab4.chess.model;

import it.polimi.ingsw.lab4.chess.common.observer.BaseObservable;
import it.polimi.ingsw.lab4.chess.common.observer.Event;
import it.polimi.ingsw.lab4.chess.model.pieces.King;
import it.polimi.ingsw.lab4.chess.model.pieces.Piece;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by marcofunaro on 4/21/15.
 * Modified by fabriziofrasca on 5/17/16.
 */
public class ChessBoard extends BaseObservable{
    public static final Position STARTING_POSITION = new Position('D', 4);
    private final Map<Position, Piece> pieces;

    public ChessBoard() {
        this.pieces = new HashMap<Position, Piece>();
        pieces.put(STARTING_POSITION, new King(Color.BLACK));
    }

    public void move(Position from, Position to){
        if(pieces.containsKey(from)
                && pieces.get(from).canMove(from, to)
                && !from.equals(to)){
            Piece king = pieces.get(from);
            pieces.remove(from);
            pieces.put(to, king);
            notify(new Event(String.format("Now the king is in position %s", to.toString())));
            
        } else if (!pieces.containsKey(from)) {
        	super.notify(new Event("no piece in the from position!!"));
        } else if (from.equals(to)){
            super.notify(new Event("final target position must differ from the initial one!!"));
        } else {
        	super.notify(new Event("the piece you're trying to move cannot reach that position!!"));
        }
    }
}
