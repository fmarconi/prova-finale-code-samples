package it.polimi.ingsw.lab4.chess.model.pieces;

import it.polimi.ingsw.lab4.chess.model.Color;
import it.polimi.ingsw.lab4.chess.model.Position;

/**
 * Created by marcofunaro on 4/21/15.
 */
public interface Piece {
    Boolean canMove(Position from, Position to);
    Color getColor();
}
