package it.polimi.ingsw.lab4.chess.model;

import it.polimi.ingsw.lab4.chess.model.exceptions.BadPositionException;

/**
 * Created by marcofunaro on 4/21/15.
 */
public class Position {
    private final Character col;
    private final Integer row;

    public Position(Character col, Integer row) {
        if(row < 1 || row >8) throw new BadPositionException(String.format("'%d' row does not exist on the chessboard", row));
        if(col < 'A' || col > 'H') throw new BadPositionException(String.format("'%c' column does not exist on the chessboard", col));
        this.col = col;
        this.row = row;
    }

    public Character getCol() {
        return col;
    }

    public Integer getRow() {
        return row;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        if (col != null ? !col.equals(position.col) : position.col != null) return false;
        return !(row != null ? !row.equals(position.row) : position.row != null);

    }

    @Override
    public int hashCode() {
        int result = col != null ? col.hashCode() : 0;
        result = 31 * result + (row != null ? row.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        return "Position(" +
                 col +
                ", " + row +
                ')';
    }
}
