package it.polimi.ingsw.lab4.chess.view;

import it.polimi.ingsw.lab4.chess.controller.Controller;
import it.polimi.ingsw.lab4.chess.model.exceptions.ChessException;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by marcofunaro on 4/18/15.
 * Modified by fabriziofrasca on 5/17/16.
 */
public class CommandReader {

    public static final Pattern PATTERN = Pattern.compile("move: (.);(.) -> (.);(.)");
    private final Controller controller;

    public CommandReader(Controller controller) {
        this.controller = controller;
    }

    public void startReading(){
        System.out.println("Moves must have this shape: \"move: D;4 -> D;5\" \n the king is, by default in position D;4");
        String command = "";
        Scanner sc = new Scanner(System.in);
        Matcher m;
        while(true){
            command = sc.nextLine();
            m = PATTERN.matcher(command);
            if(m.matches()){
                move(m);
            } else {
                if (command.equalsIgnoreCase("exit")) {
                	sc.close();
                    return;
                } else {
                    System.out.println("command unknown");
                }
            }

        }
    }

    private void move(Matcher m) {
        try {
            controller.move(m.group(1), m.group(2), m.group(3), m.group(4));
        } catch (ChessException e){
            System.out.println(e.getMessage());
        }
    }
}
