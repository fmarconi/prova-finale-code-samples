package it.polimi.ingsw.lab4.chess.common.observer;

/**
 * Created by marcofunaro on 4/18/15.
 */
public interface Observer {
    void update(Event event);
}
