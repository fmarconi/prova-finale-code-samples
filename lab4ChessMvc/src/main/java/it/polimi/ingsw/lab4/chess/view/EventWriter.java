package it.polimi.ingsw.lab4.chess.view;

import it.polimi.ingsw.lab4.chess.common.observer.Event;
import it.polimi.ingsw.lab4.chess.common.observer.Observer;


/**
 * Created by marcofunaro on 4/18/15.
 */
public class EventWriter implements Observer {

    public void update(Event event) {
        System.out.println(event.getMsg());
    }
}
