package it.polimi.ingsw.lab4.chess.pieces;

import org.junit.Test;

import it.polimi.ingsw.lab4.chess.model.Color;
import it.polimi.ingsw.lab4.chess.model.Position;
import it.polimi.ingsw.lab4.chess.model.pieces.King;
import it.polimi.ingsw.lab4.chess.model.pieces.Piece;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by marcofunaro on 4/18/15.
 */
public class KingTest {

    public static final Piece KING = new King(Color.BLACK);

    @Test
    public void testNonLegitVerticallMovesShouldNotBeAllowed() {
        assertThat(KING.canMove(new Position('A', 1), new Position('A', 3)), is(false));

    }

    @Test
    public void testNonLegitHorizontalMovesShouldNotBeAllowed() throws Exception {
        assertThat(KING.canMove(new Position('A', 1), new Position('C', 1)), is(false));
    }

    @Test
    public void testNonLegitDiagonalMovesShouldNotBeAllowed() throws Exception {
        assertThat(KING.canMove(new Position('D', 4), new Position('B', 2)), is(false));
    }


}