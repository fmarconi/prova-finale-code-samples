package it.polimi.ingsw.lab4.chess.model;

import it.polimi.ingsw.lab4.chess.model.Position;
import it.polimi.ingsw.lab4.chess.model.exceptions.BadPositionException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by marcofunaro on 4/18/15.
 */
public class PositionTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testPositionShouldBeProperlyCreated() {
        Position p = new Position('A', 3);
        assertThat(p.getCol(), is('A'));
        assertThat(p.getRow(), is(3));
    }

    @Test
    public void testUnacceptableColumnShouldThrow(){
        thrown.expect(BadPositionException.class);
        thrown.expectMessage(containsString("column does not exist on the chessboard"));
        new Position('Z', 5);
    }

    @Test
    public void testUnacceptableRowShouldThrow() throws Exception {
        thrown.expect(BadPositionException.class);
        thrown.expectMessage(containsString("row does not exist on the chessboard"));
        new Position('A', 25);
    }
}
