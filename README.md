# Prova Finale 2015/2016 - Code Samples #

This repository contains the code presented during lab sessions.

### Content List ###

* **Lab 3 - Prisoners:** MVC example using Observer pattern between Model and View and between View and Controller.
* **Lab 4 - Chess:** MVC example with direct reference of the Controller in the View and Observer pattern between Model and View.

#### Disclaimer ####
This code is only intended to illustrate the concepts we dealt with during the course. For this reason some aspects have been developed quickly without following all the best practices we mentioned so far.
Clone it, play with it but always take it critically.