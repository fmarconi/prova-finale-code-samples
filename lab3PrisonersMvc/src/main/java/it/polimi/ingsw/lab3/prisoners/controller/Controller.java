package it.polimi.ingsw.lab3.prisoners.controller;

import it.polimi.ingsw.lab3.prisoners.Observer;
import it.polimi.ingsw.lab3.prisoners.model.Model;
import it.polimi.ingsw.lab3.prisoners.model.actions.Action;
import it.polimi.ingsw.lab3.prisoners.view.View;

public class Controller implements Observer<Action> {

	private final Model gioco;

	public Controller(Model gioco, View view) {
		this.gioco = gioco;
		view.registerObserver(this);
	}

	public  void update(Action action) {
		Observer.super.update(action);
		action.esegui(this.gioco);
	}

	public void update() {

	}
}