package it.polimi.ingsw.lab3.prisoners.model.components;

public enum Switch {
	ON, OFF
}
