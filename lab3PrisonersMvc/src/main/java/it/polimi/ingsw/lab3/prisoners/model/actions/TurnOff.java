package it.polimi.ingsw.lab3.prisoners.model.actions;

import it.polimi.ingsw.lab3.prisoners.model.Model;
import it.polimi.ingsw.lab3.prisoners.model.components.Switch;

public class TurnOff extends Action{

	public TurnOff() {
		
	}

	@Override
	public void esegui(Model gioco) {
		gioco.setInterruttore(Switch.OFF);
		this.changePrigioniero(gioco);
	}
}
