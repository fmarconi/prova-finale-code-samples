package it.polimi.ingsw.lab3.prisoners.model.components;

public enum State {
	RUNNING, WIN, LOSE
}