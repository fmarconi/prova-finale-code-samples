package it.polimi.ingsw.lab3.prisoners.model.actions;

import it.polimi.ingsw.lab3.prisoners.model.Model;
import it.polimi.ingsw.lab3.prisoners.model.components.State;

public class Scommetti extends Action {

	public Scommetti() {
	}

	@Override
	public void esegui(Model gioco) {
		if (gioco.prigionieriInTheRoom().containsAll(gioco.getPrigionieri())) {
			gioco.setState(State.WIN);
		} else {
			gioco.setState(State.LOSE);
		}
	}
}