package it.polimi.ingsw.lab3.prisoners.view;

import it.polimi.ingsw.lab3.prisoners.Observable;
import it.polimi.ingsw.lab3.prisoners.Observer;
import it.polimi.ingsw.lab3.prisoners.model.Model;
import it.polimi.ingsw.lab3.prisoners.model.actions.Action;
import it.polimi.ingsw.lab3.prisoners.model.actions.Scommetti;
import it.polimi.ingsw.lab3.prisoners.model.actions.TurnOff;
import it.polimi.ingsw.lab3.prisoners.model.actions.TurnOn;
import it.polimi.ingsw.lab3.prisoners.model.changes.Change;
import it.polimi.ingsw.lab3.prisoners.model.components.State;
import it.polimi.ingsw.lab3.prisoners.model.components.Switch;

public class View extends Observable<Action> implements Observer<Change> {

	private final Model gioco;
	public View(Model gioco) {
		gioco.registerObserver(this);
		System.out.println(gioco);
		this.gioco=gioco;
	}

	public void input(String input) {
		System.out.println("I am the view I am notifying my observers");
		Action azione;
		if (input.equals(Switch.ON.toString())) {
			azione = new TurnOn();
		} else {
			if (input.equals(Switch.OFF.toString())) {
				azione = new TurnOff();
			} else {
				
				azione = new Scommetti();
			}
		}
		
		this.notifyObservers(azione);
	}

	public void update() {

		
		System.out.println("I am the view I have been notified by the model ");
		System.out.println(gioco);
	}

	public void update(Change change) {
		Observer.super.update(change);
		System.out.println("I am the view I have been notified by the model with an update C");
		
		if (change.equals(State.WIN)) {
			System.out.println("Avete vinto");
		} else {
			System.out.println("Siete morti");
		}
		System.exit(0);

	}

}