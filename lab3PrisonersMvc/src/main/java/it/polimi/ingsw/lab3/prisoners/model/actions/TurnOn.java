package it.polimi.ingsw.lab3.prisoners.model.actions;

import it.polimi.ingsw.lab3.prisoners.model.Model;
import it.polimi.ingsw.lab3.prisoners.model.components.Switch;

public class TurnOn extends Action{

	public TurnOn() {
	}

	@Override
	public void esegui(Model gioco) {
		gioco.setInterruttore(Switch.ON);
		this.changePrigioniero(gioco);
	}
}
