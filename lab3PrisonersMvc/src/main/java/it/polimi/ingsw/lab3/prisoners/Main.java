package it.polimi.ingsw.lab3.prisoners;

import java.io.IOException;
import java.util.Scanner;

import it.polimi.ingsw.lab3.prisoners.controller.Controller;
import it.polimi.ingsw.lab3.prisoners.model.Model;
import it.polimi.ingsw.lab3.prisoners.view.View;

public class Main {

	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(System.in);
		Model gioco=new Model();
		View view =new View(gioco);
		Controller controller=new Controller(gioco, view);
		
		while(true){
			System.out.println("Dimmi il comando ON/OFF ");
			String comando = in.nextLine();
			view.input(comando);
		}
	}
}