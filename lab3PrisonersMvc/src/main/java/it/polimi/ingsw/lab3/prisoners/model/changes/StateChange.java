/**
 * 
 */
package it.polimi.ingsw.lab3.prisoners.model.changes;

import it.polimi.ingsw.lab3.prisoners.model.components.State;

/**
 * @author Claudio Menghi
 *
 */
public class StateChange extends Change {

	private final State newState;
	
	public StateChange(State newState){
		this.newState=newState;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "StateChange [newState=" + newState + "]";
	}
	
	
}
