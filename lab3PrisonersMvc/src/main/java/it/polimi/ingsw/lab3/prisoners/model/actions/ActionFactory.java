package it.polimi.ingsw.lab3.prisoners.model.actions;

import it.polimi.ingsw.lab3.prisoners.model.components.Switch;

public class ActionFactory {

	public Action createActions(String input){
		String inputUPPER=input.toUpperCase();
		Action azione;
		if (inputUPPER.equals(Switch.ON.toString())) {
			azione = new TurnOn();
		} else {
			if (inputUPPER.equals(Switch.OFF.toString())) {
				azione = new TurnOff();
			} else {
				azione = new Scommetti();
			}
		}
		return azione;
	}
}
